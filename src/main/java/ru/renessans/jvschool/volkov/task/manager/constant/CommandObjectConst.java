package ru.renessans.jvschool.volkov.task.manager.constant;

import ru.renessans.jvschool.volkov.task.manager.model.TerminalCommand;

public interface CommandObjectConst {

    TerminalCommand HELP_COMMAND = new TerminalCommand(
            CommandConst.CMD_HELP, CommandConst.ARG_HELP, DescriptionConst.HELP_DESCRIPTION
    );

    TerminalCommand VERSION_COMMAND = new TerminalCommand(
            CommandConst.CMD_VERSION, CommandConst.ARG_VERSION, DescriptionConst.VERSION_DESCRIPTION
    );

    TerminalCommand ABOUT_COMMAND = new TerminalCommand(
            CommandConst.CMD_ABOUT, CommandConst.ARG_ABOUT, DescriptionConst.ABOUT_DESCRIPTION
    );

    TerminalCommand INFO_COMMAND = new TerminalCommand(
            CommandConst.CMD_INFO, CommandConst.ARG_INFO, DescriptionConst.INFO_DESCRIPTION
    );

    TerminalCommand EXIT_COMMAND = new TerminalCommand(
            CommandConst.CMD_EXIT, null, DescriptionConst.EXIT_DESCRIPTION
    );

}