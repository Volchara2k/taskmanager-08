package ru.renessans.jvschool.volkov.task.manager.printer;

import ru.renessans.jvschool.volkov.task.manager.constant.CommandConst;
import ru.renessans.jvschool.volkov.task.manager.constant.DeveloperConst;
import ru.renessans.jvschool.volkov.task.manager.constant.NotificationConst;
import ru.renessans.jvschool.volkov.task.manager.constant.CommandObjectConst;
import ru.renessans.jvschool.volkov.task.manager.util.SystemMonitor;

import java.util.Scanner;

final public class TerminalPrinter implements CommandConst, NotificationConst, DeveloperConst, CommandObjectConst {

    public void print(final String... args) {
        if (isEmptyArgs(args)) {
            commandPrintLoop();
        } else {
            final String arg = args[0];
            System.out.println(printableByArg(arg));
        }
    }

    private boolean isEmptyArgs(final String... args) {
        return args == null || args.length < 1;
    }

    private void commandPrintLoop() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            System.out.println((String.format(ENTER_FORMAT_MSG_FACTOR, CMD_EXIT, CMD_HELP)));
            command = scanner.nextLine();
            System.out.println(printableByCmd(command));
        }
    }

    private String printableByCmd(final String cmd) {
        if (isEmptyFactor(cmd))
            return NO_COMMAND_MSG;

        switch (cmd) {
            case CMD_HELP:
                return String.format(FORMAT_MSG_CMD_HELP,
                        VERSION_COMMAND, ABOUT_COMMAND, HELP_COMMAND, INFO_COMMAND, EXIT_COMMAND);
            case CMD_VERSION:
                return VERSION_MSG;
            case CMD_ABOUT:
                return String.format(FORMAT_MSG_ABOUT, DEVELOPER, DEVELOPER_MAIL);
            case CMD_INFO:
                return SystemMonitor.getInstance().formatInfo();
            case CMD_EXIT:
                return EXIT_MSG;
            default:
                return String.format(FORMAT_MSG_UNKNOWN, cmd);
        }
    }

    private String printableByArg(final String arg) {
        if (isEmptyFactor(arg))
            return NO_COMMAND_MSG;

        switch (arg) {
            case ARG_HELP:
                return String.format(FORMAT_MSG_ARG_HELP,
                        VERSION_COMMAND, ABOUT_COMMAND, INFO_COMMAND, HELP_COMMAND);
            case ARG_VERSION:
                return VERSION_MSG;
            case ARG_ABOUT:
                return String.format(FORMAT_MSG_ABOUT, DEVELOPER, DEVELOPER_MAIL);
            case ARG_INFO:
                return SystemMonitor.getInstance().formatInfo();
            default:
                return String.format(FORMAT_MSG_UNKNOWN, arg);
        }
    }

    private boolean isEmptyFactor(final String factor) {
        return factor == null || factor.isEmpty();
    }

}