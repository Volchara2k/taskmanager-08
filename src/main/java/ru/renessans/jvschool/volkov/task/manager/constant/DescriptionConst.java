package ru.renessans.jvschool.volkov.task.manager.constant;

public interface DescriptionConst {

    String VERSION_DESCRIPTION = "вывод версии программы";

    String HELP_DESCRIPTION = "вывод информации о разработчике";

    String ABOUT_DESCRIPTION = "вывод списка команд";

    String INFO_DESCRIPTION = "вывод информации о системе";

    String EXIT_DESCRIPTION = "закрыть приложение";

}