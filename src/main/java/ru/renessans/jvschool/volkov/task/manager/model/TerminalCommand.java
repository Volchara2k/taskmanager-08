package ru.renessans.jvschool.volkov.task.manager.model;

public class TerminalCommand {

    private String cmd = "";

    private String arg = "";

    private String description = "";

    public TerminalCommand() {
    }

    public TerminalCommand(String cmd, String arg, String description) {
        this.cmd = cmd;
        this.arg = arg;
        this.description = description;
    }

    public String getCmd() {
        return this.cmd;
    }

    public void setCmd(final String cmd) {
        this.cmd = cmd;
    }

    public String getArg() {
        return this.arg;
    }

    public void setArg(final String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (this.cmd != null && !this.cmd.isEmpty())
            result.append("cmd: ").append(this.cmd);
        if (this.arg != null && !this.arg.isEmpty())
            result.append(", arg: ").append(this.arg);
        if (this.description != null && !this.description.isEmpty())
            result.append("\n\t - ").append(this.description);
        return result.toString();
    }

}