package ru.renessans.jvschool.volkov.task.manager.constant;

public interface NotificationConst {

    String FORMAT_MSG_UNKNOWN = "Неизвестная команда: %s";

    String FORMAT_MSG_CMD_HELP = "%s; \n%s; \n%s; \n%s; \n%s.";

    String FORMAT_MSG_ARG_HELP = "%s; \n%s; \n%s; \n%s.";

    String FORMAT_MSG_INFO =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    String FORMAT_MSG_ABOUT = "%s - разработчик; \n%s - почта.";

    String ENTER_FORMAT_MSG_FACTOR =
            "\nВведите команду для получения данных о приложении.\n" +
                    "\tВведите команду \"%s\" для выхода из приложения, \"%s\" - для получения справки.\n";

    String VERSION_MSG = "Версия: 1.0.8.";

    String EXIT_MSG = "Выход из приложения!";

    String NO_COMMAND_MSG = "Входная команда отсутствует!";

}