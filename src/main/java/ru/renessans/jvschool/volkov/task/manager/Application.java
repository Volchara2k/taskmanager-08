package ru.renessans.jvschool.volkov.task.manager;

import ru.renessans.jvschool.volkov.task.manager.printer.TerminalPrinter;

public class Application {

    public static void main(final String[] args) {
        final TerminalPrinter printer = new TerminalPrinter();
        printer.print(args);
    }

}