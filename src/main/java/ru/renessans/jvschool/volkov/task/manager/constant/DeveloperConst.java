package ru.renessans.jvschool.volkov.task.manager.constant;

public interface DeveloperConst {

    String DEVELOPER = "Valery Volkov";

    String DEVELOPER_MAIL = "volkov.valery2013@yandex.ru";

}