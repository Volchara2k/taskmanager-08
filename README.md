# TaskManager-08

# О проекте
**Менеджер задач**

### Стек технологий:
Java SE/Spring/SPRING REST/SPRING CLOUD/SPRING TESTING/SPRING SECURITY/
SPRING SOAP/SPRING AOP/SPRING IOC/Docker/Git/MySql/PostgreSql/Maven 3/GRADLE/
HIBERNATE

### Программные требования:
- JDK 1.8;
- MS Windows 10 x64.

### Аппаратные требования:
- Процессор: Intel® Dual-Core 2.4 GHz - или аналог;
- Оперативная память: 1.5 GB; 
- Место на диске: менее 5 МБ.

### Сведения о разработчике:
**ФИО**: Волков Валерий Сергеевич

**Электронная почта**: volkov.valery2013@yandex.ru

### Команда для сборки приложения:
```bash
mvn clean install
```

### Команда для запуска приложения:
```bash
java -jar ./taskmanager.jar
```
Приложение поддерживает следующие команды/программные аргументы для получения данных о приложении:
- cmd: help, arg: -h - для получения справки;
- cmd: version, arg: -v - для получения версии приложения;
- cmd: about, arg: -a - для получения информации о разработчике;
- cmd: info, arg: -i - для вывода информации о системе;
- cmd: exit - для выхода из приложения.

### Текущая сборка CI / CD:
https://gitlab.com/Volchara2k/taskmanager-08/-/pipelines

## Скриншоты:
https://drive.google.com/drive/folders/1oDhSRSg5sKi8CL8BYPT2PrbcUFIxkbzh?usp=sharing